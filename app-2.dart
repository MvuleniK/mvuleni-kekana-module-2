void main() {

  List<String> appwinners = [
    "FNB Banking App",
    "Snapscan",
    "Live Inspect",
    "WumDrop",
    "Domestly",
    "Shyft",
    "Khula",
    "Naked Insurance",
    "EasyEquities",
    "Ambani Africa"
  ];


  List<String> copy = appwinners;


  appwinners.sort(
    (x, y) {
      return x.compareTo(y);
    },
  );

  var appwinners_2017 = copy[5];
  var appwinners_2018 = copy[6];
  var winnersTotal = appwinners.length;


  print("Sorted list is: $appwinners");
  print("The winning app of 2017 is: $appwinners_2017 \nThe winning app of 2018 is: $appwinners_2018");
  print("The total number of apps in the array is $winnersTotal.");
}
