
class MTNAPP {
  String? app_name;
  String? app_category;
  String? app_developer;
  int? year_winner;


  MTNAPP(name, category, developer, year) {
    app_name = name;
    app_category = category;
    app_developer = developer;
    year_winner = year;
  }

  
  void printAppInfo() {
    print(
        "App name: $app_name \nApp Caregory: $app_category \nApp Developer: $app_developer \nYear award won: $year_winner \n");
  }


  void nameToCaps() {
    if (app_name is String) {
      app_name = app_name!.toUpperCase();
    }
  }
}

void main() {

  var snapscan = new MTNAPP(
      "Snapscan", "Overall Winner", "Estiaan (and four others)", 2013);

  snapscan.printAppInfo();
  snapscan.nameToCaps();
  print(snapscan.app_name);
}
